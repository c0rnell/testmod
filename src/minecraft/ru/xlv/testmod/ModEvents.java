package ru.xlv.testmod;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.ForgeSubscribe;
import org.lwjgl.opengl.GL11;

public class ModEvents {

    private long time;
    private boolean flag;

    @ForgeSubscribe
    public void event(CustomEvent event) {
        System.out.println(event.getString());
    }

    @ForgeSubscribe
    public void event(RenderGameOverlayEvent.Pre event) {
        if(event.type == RenderGameOverlayEvent.ElementType.HEALTH) {
            GL11.glColor4f(0, 1, 0, 1);
            if(!flag) {
                time = System.currentTimeMillis();
                flag = true;
            }
            if(System.currentTimeMillis() - time > 10000L) {
                event.setCanceled(true);
            }
        }
    }
}
