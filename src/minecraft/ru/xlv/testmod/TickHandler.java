package ru.xlv.testmod;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.testmod.item.Items;

import java.util.EnumSet;

public class TickHandler implements ITickHandler {

    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData) {}

    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData) {
//        if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
//            EntityPlayer entityPlayer = (EntityPlayer) tickData[0];
//            Item item = Items.itemTest;
//            entityPlayer.inventory.mainInventory[10] = new ItemStack(item, 33, 0);
//            for (int i = 0; i < entityPlayer.inventory.mainInventory.length; i++) {
//                if(entityPlayer.inventory.mainInventory[i] != null && entityPlayer.inventory.getCurrentItem() != null
//                        && entityPlayer.inventory.mainInventory[i].itemID == entityPlayer.inventory.getCurrentItem().itemID) {
//
//                }
//            }
//        }
    }

    @Override
    public EnumSet<TickType> ticks() {
        return EnumSet.of(TickType.PLAYER);
    }

    @Override
    public String getLabel() {
        return MainMod.MODID;
    }
}
