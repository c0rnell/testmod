package ru.xlv.testmod.item;

import cpw.mods.fml.common.registry.GameRegistry;

public class Items {

    public static ItemBase itemTest = new ItemBase(1000, "itemTest");

    public static void register() {
        regItem(itemTest);
    }

    private static void regItem(ItemBase item) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
    }
}
