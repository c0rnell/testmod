package ru.xlv.testmod.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

class ItemBase extends Item {

    ItemBase(int itemID, String name) {
        super(itemID);
        setUnlocalizedName(name);
        setTextureName(name);
        setCreativeTab(CreativeTabs.tabMisc);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
        if(!par2World.isRemote) {
            //20 ticks = 1 sec
            par3EntityPlayer.addPotionEffect(new PotionEffect(Potion.confusion.id, 20, 1));
        }
        String message = String.format("%s, hello! Item is %s, his count is %s.", par3EntityPlayer.getCommandSenderName(),
                par1ItemStack.itemID, par1ItemStack.stackSize);
        par3EntityPlayer.addChatMessage(message);

        return super.onItemRightClick(par1ItemStack, par2World, par3EntityPlayer);
    }
}
