package ru.xlv.testmod;

import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class CustomEvent extends Event {

    private String string;

    public CustomEvent(String string) {
        this.string = string;
    }

    public void setString(String s) {
        string = s;
    }

    public String getString() {
        return string;
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}
