package ru.xlv.testmod.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import ru.xlv.testmod.MainMod;

import java.util.Random;

public class BlockBase extends Block {

    @SideOnly(Side.CLIENT)
    private Icon iconBlockTop;

    @SideOnly(Side.CLIENT)
    private Icon iconBlockSide;

    public BlockBase(int par1, String name) {
        this(par1, Material.rock);
        setTextureName(name);
        setUnlocalizedName(name);
    }

    public BlockBase(int par1, Material par2Material) {
        super(par1, par2Material);
        setCreativeTab(CreativeTabs.tabMisc);
    }

    @Override
    public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity) {
        super.onEntityWalking(par1World, par2, par3, par4, par5Entity);
        if(!par1World.isRemote) {
            if(par5Entity instanceof EntityPlayer) {
//                ((EntityPlayer) par5Entity).inventory;

                par5Entity.setPosition(100, 0, 100);
            }
        }
    }

    public int idDropped(int par1, Random par2Random, int par3) {
        return Block.dirt.idDropped(0, par2Random, par3);
    }

    /**
     * When this method is called, your block should register all the icons it needs with the given IconRegister. This
     * is the only chance you get to register icons.
     */
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister) {
        this.blockIcon = this.iconBlockSide = par1IconRegister.registerIcon(MainMod.MODID + ":" + this.getTextureName() + "_side");
        this.iconBlockTop = par1IconRegister.registerIcon(this.getTextureName() + "_top");
    }

    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    @SideOnly(Side.CLIENT)
    public Icon getIcon(int par1, int par2)
    {
        return par1 == 1 ? this.iconBlockTop : (par1 == 0 ? this.iconBlockTop : (par1 != par2 ? this.blockIcon : this.iconBlockSide));
    }

    @SideOnly(Side.CLIENT)
    public Icon getBlockTexture(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        if (par5 == 1) {
            return this.iconBlockTop;
        } else if (par5 == 0) {
            return this.iconBlockSide;
        } else {
            return this.blockIcon;
        }
    }
}
