package ru.xlv.testmod.block;

import cpw.mods.fml.common.registry.GameRegistry;
import ru.xlv.testmod.block.BlockBase;

public class Blocks {

    public static BlockBase blockTest = new BlockBase(1001, "blockTest");

    public static void register() {
        regBlock(blockTest);
    }

    private static void regBlock(BlockBase block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
