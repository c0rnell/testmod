package ru.xlv.testmod;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.testmod.block.Blocks;
import ru.xlv.testmod.item.Items;

@Mod(
        name = "Test Mod",
        modid = MainMod.MODID,
        version = "1.0"
)
public class MainMod {

    public static final String MODID = "testmod";

    @Mod.Instance(MODID)
    public static MainMod mainMod;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        Blocks.register();
        Items.register();
        MinecraftForge.EVENT_BUS.register(new ModEvents());
        TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);

//        String pre = "---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;---------------------------dasdasddaddaklhalghnj;gadfhngjdfkl;fadjk ar;ojg najgnja;;gjkla bnjdklgajkgbjkagjkgbjgb;gbj;";
//        CustomEvent customEvent = new CustomEvent(pre);
//        MinecraftForge.EVENT_BUS.post(customEvent);
    }

    @Mod.EventHandler
    public void event(FMLPostInitializationEvent event) {

    }

    @Mod.EventHandler
    public void event(FMLPreInitializationEvent event) {

    }
}
